/*
This file is part of the Everdream Runner (https://gitlab.com/ev3rdream/runner).
Copyright (c) 2022 Everdream.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

use std::process::exit;
use std::{
    io::{self, Stdout},
    time::Duration,
};

use async_std::task::JoinHandle;
use async_std::{
    channel,
    channel::{Receiver, Sender},
    task,
};
use crossterm::event::poll;
use crossterm::{
    event::{Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use log::{debug, error, info};
use tui::{
    backend::CrosstermBackend,
    layout::{Constraint, Corner, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, Borders, List, ListItem, Tabs},
    Terminal,
};

use crate::tui_state::{Severity, TabState, TuiEvent, TuiState};

// terminal type to be passed around
type TerminalT = Terminal<CrosstermBackend<Stdout>>;

// tui thread. first to start, last to quit
pub(crate) fn run(start_tui: bool) -> (Sender<TuiEvent>, JoinHandle<()>) {
    let (tx_tui, rx_tui) = channel::unbounded();
    task::spawn(start_key_monitoring_loop(tx_tui.clone()));
    let tui_handle = task::spawn(start_display_loop(rx_tui, start_tui));
    (tx_tui, tui_handle)
}

// monitor key presses from users
async fn start_key_monitoring_loop(tx: Sender<TuiEvent>) {
    loop {
        if let Ok(true) = poll(Duration::from_millis(100)) {
            if let Ok(event) = crossterm::event::read() {
                tx.try_send(TuiEvent::Input(event))
                    .expect("Sending to tui should never fail!");
            }
        } else {
            tx.try_send(TuiEvent::Tick)
                .expect("Sending to tui should never fail!");
        }
    }
}

// react to the events
async fn start_display_loop(rx: Receiver<TuiEvent>, start_tui: bool) {
    let mut tui_state = TuiState::build(Vec::new());
    let mut terminal = None;
    if start_tui && enable_raw_mode().is_ok() {
        let mut stdout = io::stdout();
        if execute!(stdout, EnterAlternateScreen).is_ok() {
            let backend = CrosstermBackend::new(stdout);
            terminal = Terminal::new(backend).ok();
            info!("Starting Backend");
        }
    }
    loop {
        if update_tui_state(&mut tui_state, &rx).await {
            if let Some(term) = &mut terminal {
                let _ = term.clear();
                let mut stdout = io::stdout();
                let _ = execute!(stdout, LeaveAlternateScreen);
                let _ = disable_raw_mode();
                let _ = term.show_cursor();
            }
            exit(0);
        }
        if let Some(term) = &mut terminal {
            draw_screen(&mut tui_state, term);
        }
    }
}

// updates tui data based on input or times out silently in 250 ms
async fn update_tui_state(tui_state: &mut TuiState, rx: &Receiver<TuiEvent>) -> bool {
    if let Ok(event) = rx.recv().await {
        match event {
            TuiEvent::TabListChanged(titles) => {
                tui_state.tabs = titles
                    .iter()
                    .map(|title| TabState::build(title.clone()))
                    .collect();
                info!(
                    "tabs updated:\n| {}",
                    tui_state
                        .tabs
                        .iter()
                        .map(|tab| tab.title.clone() + " | ")
                        .collect::<String>()
                );
            }
            TuiEvent::CommandStarted(idx) => {
                tui_state.tabs[idx].add_message(Severity::System, String::from("Command Started"));
                info!("{}: command started", tui_state.tabs[idx].title);
            }
            TuiEvent::NewStdoutMessage(idx, message) => {
                tui_state.tabs[idx].add_message(Severity::Info, message.clone());
                info!("{}: {}", tui_state.tabs[idx].title, message);
            }
            TuiEvent::NewStderrMessage(idx, message) => {
                tui_state.tabs[idx].add_message(Severity::Error, message.clone());
                debug!("{}: {}", tui_state.tabs[idx].title, message);
            }
            TuiEvent::CommandError(idx) => {
                tui_state.tabs[idx]
                    .add_message(Severity::Error, String::from("Command could not be run"));
                error!("{}: command could not be run!", tui_state.tabs[idx].title);
            }
            TuiEvent::CommandEnded(idx) => {
                tui_state.tabs[idx].add_message(Severity::System, String::from("Command ended"));
                info!("{}: command ended", tui_state.tabs[idx].title);
            }
            TuiEvent::Input(Event::Key(key)) => match key.code {
                KeyCode::Right => tui_state.next(),
                KeyCode::Left => tui_state.previous(),
                KeyCode::Char('q') => return true,
                _ => {}
            },
            TuiEvent::ProcessEnded(idx) => {
                tui_state.tabs[idx].add_message(Severity::Info, String::from("Process ended"));
                tui_state.tabs[idx].finished = true;
                info!("{}: process finished", tui_state.tabs[idx].title);
            }
            _ => {}
        }
    }
    false
}

// draw on display
fn draw_screen(tui_state: &mut TuiState, terminal: &mut TerminalT) {
    let _done = tui_state.tabs.iter().all(|tab| tab.finished);
    terminal
        .draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(1)
                .constraints([Constraint::Length(3), Constraint::Min(0)].as_ref())
                .split(f.size());

            let block = Block::default().style(Style::default().bg(Color::Black).fg(Color::Cyan));
            f.render_widget(block, f.size());

            let tabs = create_tabs(tui_state);
            f.render_widget(tabs, chunks[0]);

            let output = create_output(tui_state);
            f.render_widget(output, chunks[1]);
        })
        .expect("failed to start terminal display");
}

// creates tabs on top of screen
fn create_tabs(tui_state: &TuiState) -> Tabs {
    let titles = tui_state
        .tabs
        .iter()
        .map(|t| Spans::from(Span::styled(&t.title, Style::default().fg(Color::Cyan))))
        .collect();
    Tabs::new(titles)
        .block(Block::default().borders(Borders::ALL).title("Commands"))
        .select(tui_state.index)
        .style(Style::default().fg(Color::Cyan))
        .highlight_style(
            Style::default()
                .add_modifier(Modifier::BOLD)
                .fg(Color::Magenta),
        )
}

// draws output in the bottom of the screen
fn create_output(tui_state: &TuiState) -> List {
    let messages: Vec<ListItem> = tui_state.tabs[tui_state.index]
        .content
        .iter()
        .rev()
        .map(|(severity, text)| match severity {
            Severity::Info => ListItem::new(Span::styled(text, Style::default().fg(Color::White))),
            Severity::Error => {
                ListItem::new(Span::styled(text, Style::default().fg(Color::Magenta)))
            }
            Severity::System => ListItem::new(Span::styled(text, Style::default().fg(Color::Cyan))),
        })
        .collect();

    List::new(messages)
        .block(Block::default().borders(Borders::ALL).title("Output"))
        .start_corner(Corner::BottomLeft)
}
