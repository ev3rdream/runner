/*
This file is part of the Everdream Runner (https://gitlab.com/ev3rdream/runner).
Copyright (c) 2022 Everdream.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

use serde::de::Error;
use serde::{Deserialize, Deserializer};

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub(crate) struct BackupStrategy {
    // max times app can crash in a period
    pub(crate) times: u64,

    // period to check
    #[serde(deserialize_with = "deserialize_duration")]
    pub(crate) period: chrono::Duration,

    // script to run
    pub(crate) script: Option<String>,

    // arguments for safe mode
    #[serde(alias = "safe mode")]
    pub(crate) safe_mode: Option<Vec<String>>,
}

fn deserialize_duration<'de, D>(deserializer: D) -> Result<chrono::Duration, D::Error>
where
    D: Deserializer<'de>,
{
    let period = String::deserialize(deserializer)?;

    let number: i64 = period
        .get(..period.len() - 1)
        .ok_or_else(|| D::Error::custom("backup strategy period"))?
        .parse()
        .map_err(|_| D::Error::custom("backup strategy period"))?;

    if period.ends_with('s') {
        return Ok(chrono::Duration::seconds(number));
    } else if period.ends_with('m') {
        return Ok(chrono::Duration::minutes(number));
    } else if period.ends_with('h') {
        return Ok(chrono::Duration::hours(number));
    } else if period.ends_with('d') {
        return Ok(chrono::Duration::days(number));
    } else if period.ends_with('w') {
        return Ok(chrono::Duration::weeks(number));
    }

    Err(D::Error::custom("backup strategy period"))
}
