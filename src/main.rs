/*
This file is part of the Everdream Runner (https://gitlab.com/ev3rdream/runner).
Copyright (c) 2022 Everdream.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

mod backup_strategy;
mod command_config;
mod command_mode;
mod config;
mod config_error;
mod monitor_stderr;
mod monitor_stdout;
mod run_command;
mod runner;
mod runner_error;
mod tui;
mod tui_state;

use async_std::task;
use clap::{crate_version, Arg, Command};
use log::{info, LevelFilter};
use simplelog::{ColorChoice, CombinedLogger, SharedLogger, TermLogger, TerminalMode, WriteLogger};
use std::fs::File;

use crate::config::Config;
use runner_error::{Result, RunnerError};

// main function
fn main() {
    // parse command line arguments
    let config = match parse_args() {
        Ok(config) => config,
        Err(err) => {
            eprintln!("{}", err);
            return;
        }
    };

    // parse config
    let config = match Config::create(config) {
        Ok(config) => config,
        Err(err) => {
            eprintln!("{}", err);
            return;
        }
    };

    // start logging
    start_log(config.tui);
    info!("================================================================================");
    info!("\t\t\t\t\t\t\t\t\tRUNNER {}", crate_version!());
    info!("================================================================================");

    // run main part async
    task::block_on(runner::run(config));
}

// parse arguments using clap
// runner takes one mandatory argument, path to a config file
fn parse_args() -> Result<String> {
    Command::new("Runner")
        .version(crate_version!())
        .author("J. R. Lake")
        .about("Runner and monitoring application")
        .arg(
            Arg::new("config")
                .short('c')
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true)
                .required(true),
        )
        .get_matches()
        .value_of("config")
        .ok_or(RunnerError::MissingConfiguration)
        .map(|val| val.to_owned())
}

// Start log
// if tue is false, also log to console
fn start_log(tui: bool) {
    //ensure logs folder exists
    std::fs::create_dir_all("logs").expect("Creating error directory should not fail");

    // file logger
    let mut loggers: Vec<Box<dyn SharedLogger>> = vec![WriteLogger::new(
        LevelFilter::Info,
        simplelog::Config::default(),
        File::create(format!(
            "logs/runner_{}.log",
            chrono::Utc::now().format("%Y_%m_%d:%H_%M")
        ))
        .unwrap(),
    )];

    if !tui {
        loggers.push(TermLogger::new(
            LevelFilter::Info,
            simplelog::Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ))
    }

    let _ = CombinedLogger::init(loggers);
}
