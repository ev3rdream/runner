/*
This file is part of the Everdream Runner (https://gitlab.com/ev3rdream/runner).
Copyright (c) 2022 Everdream.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

use async_std::{channel::Sender, task};
use chrono::{DateTime, Utc};
use futures::future::join_all;

use crate::{
    command_config::CommandConfig, command_mode::CommandMode, config::Config, runner_error::Result,
    tui, tui_state::TuiEvent, RunnerError,
};

// main run called from main function
pub(crate) async fn run(config: Config) {
    // start tui
    let (tx_tui, tui_handle) = tui::run(config.tui);

    //ensure error folder exists
    std::fs::create_dir_all(&config.crash_path).expect("Creating error directory should not fail");

    // execute all commands, saving handles
    let command_handle = task::spawn(execute_commands(config, tx_tui));

    // execute commands
    command_handle.await.expect("executing command failed");
    tui_handle.await;
}

// executes command based on mode
async fn execute_commands(config: Config, tx: Sender<TuiEvent>) -> Result<()> {
    // set tabs
    tx.try_send(TuiEvent::TabListChanged(
        config
            .commands
            .iter()
            .map(|command| command.name.clone())
            .collect(),
    ))
    .expect("sending to tui should always succeed");

    let mut futures = Vec::new();
    for (id, command) in config.commands.into_iter().enumerate() {
        match command.mode {
            CommandMode::RunOnce => futures.push(task::spawn(run_once(
                command,
                config.crash_path.clone(),
                tx.clone(),
                id,
            ))),
            CommandMode::RunOnceAndWait => {
                run_once(command, config.crash_path.clone(), tx.clone(), id).await?
            }
            CommandMode::RunUntilSuccess => futures.push(task::spawn(run_until_success(
                command,
                config.crash_path.clone(),
                tx.clone(),
                id,
            ))),
            CommandMode::RunUntilSuccessAndWait => {
                run_until_success(command, config.crash_path.clone(), tx.clone(), id).await?
            }
            CommandMode::KeepAlive => futures.push(task::spawn(run_keep_alive(
                command,
                config.crash_path.clone(),
                tx.clone(),
                id,
            ))),
        };
    }
    join_all(futures).await;
    Ok(())
}

// run once
async fn run_once(
    command: CommandConfig,
    error_path: String,
    tx: Sender<TuiEvent>,
    id: usize,
) -> Result<()> {
    crate::run_command::run_command(&command, error_path, tx.clone(), id).await?;
    tx.try_send(TuiEvent::ProcessEnded(id))
        .map_err(RunnerError::ChannelError)
}

// run until success (exit code 0)
async fn run_until_success(
    command: CommandConfig,
    error_path: String,
    tx: Sender<TuiEvent>,
    id: usize,
) -> Result<()> {
    let mut crashes = Vec::new();
    while crate::run_command::run_command(&command, error_path.clone(), tx.clone(), id)
        .await
        .is_err()
    {
        crashes.push(Utc::now());
        if !handle_crash_strategy(&command, &crashes, &tx, id, &error_path).await? {
            return Ok(());
        }
    }
    tx.try_send(TuiEvent::ProcessEnded(id))
        .map_err(RunnerError::ChannelError)
}

// keep alive, ignoring exit codes
async fn run_keep_alive(
    command: CommandConfig,
    error_path: String,
    tx: Sender<TuiEvent>,
    id: usize,
) -> Result<()> {
    let mut crashes = Vec::new();
    loop {
        if crate::run_command::run_command(&command, error_path.clone(), tx.clone(), id)
            .await
            .is_err()
        {
            crashes.push(Utc::now());
            if !handle_crash_strategy(&command, &crashes, &tx, id, &error_path).await? {
                tx.try_send(TuiEvent::ProcessEnded(id))?;
                return Ok(());
            }
        }
    }
}

// handle backup strategy
async fn handle_crash_strategy(
    command: &CommandConfig,
    crashes: &Vec<DateTime<Utc>>,
    tx: &Sender<TuiEvent>,
    id: usize,
    error_path: &str,
) -> Result<bool> {
    if let Some(strategy) = &command.backup_strategy {
        let mut crash_count = 0u64;
        for timestamp in crashes {
            if timestamp > &(Utc::now() - strategy.period) {
                crash_count += 1u64;
            }
            if crash_count > strategy.times {
                if strategy.script.is_none() && strategy.safe_mode.is_none() {
                    // we have no handling strategy so we just give up
                    tx.try_send(TuiEvent::NewStderrMessage(
                        id,
                        String::from("Crash limit reached with no handling strategy, giving up!"),
                    ))?;

                    return Ok(false);
                }
                if let Some(script) = &strategy.script {
                    let script_config = CommandConfig {
                        command: script.to_owned(),
                        args: Vec::new(),
                        stdout_history: command.stdout_history,
                        mode: CommandMode::RunOnceAndWait,
                        name: script.to_owned(),
                        backup_strategy: None,
                    };
                    run_once(script_config, error_path.to_owned(), tx.clone(), id).await?
                }
                if let Some(args) = &strategy.safe_mode {
                    let script_config = CommandConfig {
                        command: command.command.to_owned(),
                        args: args.clone(),
                        stdout_history: command.stdout_history,
                        mode: CommandMode::RunOnceAndWait,
                        name: command.name.to_owned(),
                        backup_strategy: None,
                    };
                    run_once(script_config, error_path.to_owned(), tx.clone(), id).await?
                }
            }
        }
    }
    Ok(true)
}
