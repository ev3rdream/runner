/*
This file is part of the Everdream Runner (https://gitlab.com/ev3rdream/runner).
Copyright (c) 2022 Everdream.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

use serde::Deserialize;

// enum indicating whether app should be restarted
#[derive(Debug, Clone, Deserialize)]
pub(crate) enum CommandMode {
    // run once, never repeat
    #[serde(alias = "run once")]
    RunOnce,

    // run once, wait for finish
    #[serde(alias = "run once and wait")]
    RunOnceAndWait,

    // run until exits successfully
    #[serde(alias = "run until success")]
    RunUntilSuccess,

    // run until exits successfully, wait for finished
    #[serde(alias = "run until success and wait")]
    RunUntilSuccessAndWait,

    // restart no matter what
    #[serde(alias = "keep alive")]
    KeepAlive,
}
