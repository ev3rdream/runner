/*
This file is part of the Everdream Runner (https://gitlab.com/ev3rdream/runner).
Copyright (c) 2022 Everdream.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

use serde::Deserialize;
use std::path::Path;

use crate::{backup_strategy::BackupStrategy, command_mode::CommandMode};

// default number of lines to store for stdout history
const DEFAULT_HISTORY: usize = 1000usize;
fn default_history() -> usize {
    DEFAULT_HISTORY
}

// default mode for application if none specified
const DEFAULT_MODE: CommandMode = CommandMode::RunUntilSuccess;
fn default_mode() -> CommandMode {
    DEFAULT_MODE
}

// single app configuration
// this struct holds all information needed to successfully run a process
#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub(crate) struct CommandConfig {
    // command to execute
    pub(crate) command: String,

    // arguments to pass to command
    pub(crate) args: Vec<String>,

    // number of lines to store for stdout
    #[serde(default = "default_history", alias = "stdout history")]
    pub(crate) stdout_history: usize,

    // mode to run application in
    #[serde(default = "default_mode")]
    pub(crate) mode: CommandMode,

    // name given to application
    #[serde(default)]
    pub(crate) name: String,

    // backup strategy
    #[serde(alias = "backup strategy")]
    pub(crate) backup_strategy: Option<BackupStrategy>,
}

impl CommandConfig {
    // get name from command. should extract file name from executable path
    pub(crate) fn auto_set_name(&mut self) {
        self.name = Path::new(&self.command)
            .file_stem()
            .unwrap_or_default()
            .to_string_lossy()
            .to_string();
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    fn from_command(cmd: &str) -> CommandConfig {
        CommandConfig {
            command: cmd.to_string(),
            args: Vec::new(),
            stdout_history: DEFAULT_HISTORY,
            mode: DEFAULT_MODE,
            name: String::new(),
            backup_strategy: None,
        }
    }
    #[test]
    fn test_get_name() {
        let mut config = from_command("ls");
        config.auto_set_name();
        assert_eq!(config.name, String::from("ls"));
        let mut config = from_command("test.exe");
        config.auto_set_name();
        assert_eq!(config.name, String::from("test"));
        let mut config = from_command("path/test.exe");
        config.auto_set_name();
        assert_eq!(config.name, String::from("test"));
        let mut config = from_command("path/test");
        config.auto_set_name();
        assert_eq!(config.name, String::from("test"));
    }
}
