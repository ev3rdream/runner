/*
This file is part of the Everdream Runner (https://gitlab.com/ev3rdream/runner).
Copyright (c) 2022 Everdream.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

use clap::crate_version;

use crate::{command_config::CommandConfig, config_error::ConfigError};
use serde::Deserialize;

// default mode for application if none specified
const DEFAULT_TUI: bool = true;
fn default_tui() -> bool {
    DEFAULT_TUI
}

// All config data parsed out
#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub(crate) struct Config {
    // application (must be runner, to check that config files aren't mixed up)
    pub(crate) application: String,

    // version (must equal version of runner)
    pub(crate) version: String,

    // list of commands to execute with
    pub(crate) commands: Vec<CommandConfig>,

    // folder for storing crash reports
    #[serde(alias = "crash path")]
    pub(crate) crash_path: String,

    // log internal behavior in file
    #[serde(default = "default_tui")]
    pub(crate) tui: bool,
}

impl Config {
    // creates parsed out configuration from a path to configuration file and reports on any errors
    pub(crate) fn create(path: String) -> Result<Config, ConfigError> {
        let file = std::fs::File::open(&path)?;
        let mut config: Config = serde_json::from_reader(file)?;
        config.verify_config()?;
        Ok(config)
    }

    // verifies that config fits application name and version
    fn verify_config(&mut self) -> Result<(), ConfigError> {
        if self.application != "runner" {
            return Err(ConfigError::WrongApplicationName(self.application.clone()));
        }
        if self.version != crate_version!() {
            return Err(ConfigError::WrongVersion(self.version.clone()));
        }
        for command in self.commands.iter_mut() {
            if command.name.is_empty() {
                command.auto_set_name();
            }
        }
        Ok(())
    }
}

mod tests {

    #[test]
    fn test_basic_config() {
        let config: crate::config::Config = serde_json::from_value(serde_json::json!({
            "application": "runner",
            "version": "0.4.0",
            "crash path": "./err",
            "commands": [
                {
                    "command": "./crash_unless_arg",
                    "args": [ ],
                    "mode": "keep alive",
                    "backup strategy": {
                        "times": 3u64,
                        "period": "1m",
                        "script": "ls",
                        "safe mode": [ "-v" ]
                    }
                },
                {
                    "command": "./crash_every_10s",
                    "args": [ "-all" ],
                    "mode": "run once"
                }
            ]
        }))
        .unwrap();
        assert_eq!(config.application, "runner");
    }
}
